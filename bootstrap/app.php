<?php
/**
 * Composer autoload.
 */
require_once '../vendor/autoload.php';

/**
 * A shortcut to our configuration directory.
 */
$config_path = '../config/';

/**
 * All our configs formatted in this array.
 */
$config = array(
	'settings'	=>	require ($config_path.'settings.php'),
	'routes'	=>	require ($config_path.'routes.php'),
);

/**
 * Finally, return the Router object to the calling file.
 * Incase of no modifications, this is 'public/index.php'.
 */
return new RecAlpam\DomainRouter\System\App($config);