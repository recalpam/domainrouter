<?php
return
array(
	/**
	 * If request doesnt pass validity test, load the following file/path.
	 * If false, a generic error message will be shown along with a 403 response type in the header.
	 */
	'fallback'	=> false
);