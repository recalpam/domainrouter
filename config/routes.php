<?php
return
/**
 * The domain routes array is orchestrated in the following manner:
 * 
 * Domain -> Bootstrap file path 
 * 			(commonly 'public/index.php' of your project dir)
 * 
 * @example  array ['foobar.com'	=>	'/var/www/foobar/public/index.php']
 * 
 * @see config/settings.php if you wish to define a standard/default path/file to load.
 */
array(
	'foo.bar'	=>	'/var/www/foobar/public/index.php',
);