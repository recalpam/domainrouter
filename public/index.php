<?php
/**
 * Load all dependencies and bootstrap our router
 */
define('boottime', microtime());
$app=require('../bootstrap/app.php');
$app->route();