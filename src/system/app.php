<?php namespace RecAlpam\DomainRouter\System;



/**
 * @author RecAlpam
 * @package RecAlpam\DomainRouter
 * @see https://gitlab.com/recalpam/domainrouter
 */
class App {

	/**
	 * Contains our routes and other configuration options
	 */
	private $config;

	/**
	 * The host for the current request
	 */
	private $host;

	/**
	 * Needs a configuration array to work
	 * @param array $routes 
	 * @example  new Router(array(..));
	 */
	public function __construct(&$config){
		$this->host = $_SERVER['HTTP_HOST'];
		$this->config = $config;
	}

	/**
	 * Route the incomming request to the appropiate path
	 */
	public function route(){

		// want to mark some benches? uncomment this:
		// echo microtime()-boottime;

		/**
		 * Check validity of the request
		 */
		if( $this->valid()){
			$this->load();
		}

		/**
		 * If the request is somehow invalid, initiate fallback
		 */
		$this->fallback();
	}

	private function valid(){
		if( !isset( $this->config['routes'][$this->host])) return false;
		if( !file_exists( $this->config['routes'][$this->host])) return false;
		return true;
	}

	private function load(){
		require $this->config['routes'][$this->host];
	}

	public function fallback(){
		if( $this->config['settings']['fallback']) {
			require $this->config['settings']['fallback'];
		}else{
			echo 'Invalid request.';
		}
	}
}